import React from "react";

import CreateBlog from "../../src/features/blog/create/createBlog.container";

const CreateBlogPage = () => <CreateBlog />;

export default CreateBlogPage;
