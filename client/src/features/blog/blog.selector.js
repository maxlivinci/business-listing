const getBlogs = state => state.blog.blogs;

const getBlog = state => state.blog.blog;

export { getBlogs, getBlog };
