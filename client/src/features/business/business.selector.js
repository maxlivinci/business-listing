const getBusinesses = state => state.business.businesses;

const getBusiness = state => state.business.business;

export { getBusinesses, getBusiness };
